package com.bosko;

import static org.junit.Assert.assertEquals;

import java.util.function.Consumer;

import javax.persistence.EntityManager;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import utils.DBSchemaUtil;
import utils.PersistenceManager;
import utils.TransactionUtil;

public class SingleTableInheritanceTest {

	@Rule
    public TemporaryFolder tempFolder = new TemporaryFolder();
	
	@Before
	public void doBefore() {
		DBSchemaUtil.createDBSchemaFiles(PersistenceManager.PERSISTENCE_UNIT_NAME, tempFolder.getRoot().getAbsolutePath());
		DBSchemaUtil.applyDBSchema(PersistenceManager.PERSISTENCE_UNIT_NAME, tempFolder.getRoot().getAbsolutePath());		
	}
	
	@Test
	public void testSingleInheritance() {
		Consumer<EntityManager> block = (EntityManager em) -> {
			Company company = new Company();
			company.setName("IBM2");

			InternalDeveloper dev1 = new InternalDeveloper();
			dev1.setName("dev1");

			ExternalDeveloper dev2 = new ExternalDeveloper();
			dev2.setName("dev2");

			company.addExternal(dev2);
			company.addInternal(dev1);
			em.persist(company);

		};
		TransactionUtil.applyAndCommit(block);
		
		Consumer<EntityManager> result = (EntityManager em) -> {
			Company company = em.find(Company.class, 1);
			company.getExternalDeveloper();
			company.getInternalDeveloper();
			company.getDeveloper();

			assertEquals(1, company.getInternalDeveloper().size());
			assertEquals(1, company.getExternalDeveloper().size());
			assertEquals(2, company.getDeveloper().size());
		};
		TransactionUtil.applyAndCommit(result);
	}
	
	@Test
	public void testUpdateSchema() {
		DBSchemaUtil.createDBSchemaFiles("MainPersistenceUnit", "/home/bosko/Temp");
	}

}
