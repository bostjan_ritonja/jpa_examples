package utils;

import java.io.IOException;
import java.net.URL;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public final class PersistenceManager {

    public static final String PERSISTENCE_UNIT_NAME = "MainPersistenceUnit";
    private EntityManagerFactory emFactory;

    private PersistenceManager() {
        this.emFactory = Persistence.createEntityManagerFactory(getPersistenceUnit());
    }

	private static class PersistenceManagerHolder {
        public static PersistenceManager INSTANCE = new PersistenceManager();
    }

    public static PersistenceManager getInstance() {
        return PersistenceManagerHolder.INSTANCE;
    }

    public static EntityManager getEntityManager() {
        return getInstance().getEntityManagerFactory().createEntityManager();
    }

    private EntityManagerFactory getEntityManagerFactory() {
        return this.emFactory;
    }
    
    private String getPersistenceUnit() {
    	Properties properties = loadProperties("db.properties");
    	if(properties != null && properties.containsKey("PERSISTENCE_UNIT")) {
    		return properties.getProperty("PERSISTENCE_UNIT");
    	}
    	return PERSISTENCE_UNIT_NAME;
    }
    
    private Properties loadProperties(String propertyFileName) {
    	ClassLoader classLoader = getClass().getClassLoader();
    	URL incoming = classLoader.getResource("META-INF/" + propertyFileName);
        if (incoming != null) {
            try {
            	Properties properties = new Properties();
				properties.load(incoming.openStream());
				return properties;
			} catch (IOException e) {
				return null;
			}
        }
        return null;
    }
}
