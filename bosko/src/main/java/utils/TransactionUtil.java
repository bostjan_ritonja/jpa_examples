package utils;
import java.util.function.Consumer;
import java.util.function.Function;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TransactionUtil {

	static final Logger logger = LoggerFactory.getLogger(TransactionUtil.class);
	
    @SafeVarargs
	public static <T> void commit(T... anEntities) {
        Consumer<EntityManager> transactional = (EntityManager em) -> {
            for(T anEntity : anEntities) {
                em.persist(anEntity);
            }
        };
        applyAndCommit(transactional);
    }

    public static void applyAndCommit(Consumer<EntityManager> transactional) {
        EntityManager em = PersistenceManager.getEntityManager();
        EntityTransaction transaction = em.getTransaction();
        try {
            transaction.begin();
            transactional.accept(em);
            transaction.commit();
        } catch (Exception e) {
        	logger.error("Error at committing changes", e);
            transaction.rollback();
        } finally {
            em.close();
        }
    }

    public static <T> T applyAndReturn(Function<EntityManager, T> transactional) {
        EntityManager em = PersistenceManager.getEntityManager();
        T result = null;
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        try {
            result = transactional.apply(em);
            transaction.rollback();
        } catch (Exception e) {
        	logger.error("Error at applying changes", e);
            transaction.rollback();
        } finally {
            em.close();
        }
        return result;
    }
}
