package utils;

import javax.persistence.Persistence;
import java.util.HashMap;
import java.util.Map;

public class DBSchemaUtil {

    public static void createDBSchemaFiles(String aPersistenceUnit, String aScriptPath) {
        Persistence.generateSchema(aPersistenceUnit, createSchemaParameters(aScriptPath));
    }

    public static void applyDBSchema(String aPersistenceUnit, String aScriptPath) {
      Persistence.generateSchema(aPersistenceUnit, applySchemaParameters(aScriptPath));
    }
    
    private static Map<String, String> applySchemaParameters(String aScriptPath) {
        Map<String, String> properties = new HashMap<>();
        properties.put("javax.persistence.schema-generation.database.action", "drop-and-create");
        properties.put("javax.persistence.schema-generation.create-source", "script");
        properties.put("javax.persistence.schema-generation.create-script-source", aScriptPath + "/create.sql");
        properties.put("javax.persistence.schema-generation.drop-source", "script");
        properties.put("javax.persistence.schema-generation.drop-script-source", aScriptPath + "/drop.sql");
        properties.put("hibernate.show_sql", "true");
        return properties;
    }

    private static Map<String, String> createSchemaParameters(String aScriptPath) {
        Map<String, String> properties = new HashMap<>();
        properties.put("hibernate.hbm2ddl.delimiter", ";");
        properties.put("javax.persistence.schema-generation.scripts.action", "drop-and-create");
        properties.put("javax.persistence.schema-generation.scripts.create-target", aScriptPath + "/create.sql");
        properties.put("javax.persistence.schema-generation.scripts.drop-target", aScriptPath + "/drop.sql");
        properties.put("hibernate.show_sql", "true");
        return properties;
    }
    
    public static void applyCreateScript(String aPersistenceUnit, String aScriptPath) {
    	Map<String, String> properties = new HashMap<>();
        properties.put("javax.persistence.schema-generation.database.action", "create");
        properties.put("javax.persistence.schema-generation.create-source", "script");
        properties.put("javax.persistence.schema-generation.create-script-source", aScriptPath + "/create.sql");
        properties.put("hibernate.show_sql", "true");
        Persistence.generateSchema(aPersistenceUnit, properties);
    }
    
    public static void applyDropScript(String aPersistenceUnit, String aScriptPath) {
    	Map<String, String> properties = new HashMap<>();
        properties.put("javax.persistence.schema-generation.database.action", "drop");
        properties.put("javax.persistence.schema-generation.drop-source", "script");
        properties.put("javax.persistence.schema-generation.drop-script-source", aScriptPath + "/drop.sql");
        properties.put("hibernate.show_sql", "true");
        Persistence.generateSchema(aPersistenceUnit, properties);
    }


}
