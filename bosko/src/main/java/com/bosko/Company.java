package com.bosko;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

public class Company {

	private Integer oid;
	private String name;
	private List<InternalDeveloper> internalDeveloper = new ArrayList<>();
	private List<ExternalDeveloper> externalDeveloper = new ArrayList<>();
	private List<Developer> developer = new ArrayList<>();

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public Integer getOid() {
		return oid;
	}
	
	public void setOid(Integer oid) {
		this.oid = oid;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	@OneToMany(mappedBy = "fromInternalcompany", cascade = CascadeType.ALL)
	public List<InternalDeveloper> getInternalDeveloper() {
		return internalDeveloper;
	}

	public void setInternalDeveloper(List<InternalDeveloper> internalDeveloper) {
		this.internalDeveloper = internalDeveloper;
	}

	@OneToMany(mappedBy = "fromExternalcompany", cascade = CascadeType.ALL)
	public List<ExternalDeveloper> getExternalDeveloper() {
		return externalDeveloper;
	}

	public void setExternalDeveloper(List<ExternalDeveloper> externalDeveloper) {
		this.externalDeveloper = externalDeveloper;
	}

	@OneToMany(mappedBy = "company")
	public List<Developer> getDeveloper() {
		return developer;
	}

	public void setDeveloper(List<Developer> aDeveloper) {
		this.developer = aDeveloper;
	}
	
	public void addInternal(InternalDeveloper dev) {
		dev.setCompany(this);
		dev.setFromInternalcompany(this);
		getInternalDeveloper().add(dev);	
	}
	
	public void addExternal(ExternalDeveloper dev) {
		dev.setFromExternalcompany(this);
		dev.setCompany(this);
		getExternalDeveloper().add(dev);	
	}
	
	public void addJava(Developer dev) {
		dev.setCompany(this);
		getDeveloper().add(dev);	
	}
	
}
