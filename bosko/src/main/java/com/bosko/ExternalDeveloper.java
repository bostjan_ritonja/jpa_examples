package com.bosko;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue(value = "EXTERNAL")
public class ExternalDeveloper extends Developer {
	
	private Company fromExternalcompany;

	@ManyToOne
	public Company getFromExternalcompany() {
		return fromExternalcompany;
	}

	public void setFromExternalcompany(Company fromExternalcompany) {
		this.fromExternalcompany = fromExternalcompany;
	}

	
}
