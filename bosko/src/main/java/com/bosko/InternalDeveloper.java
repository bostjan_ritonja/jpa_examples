package com.bosko;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@Entity
@DiscriminatorValue(value = "INTERNAL")
public class InternalDeveloper extends Developer {

	private Company fromInternalcompany;

	@ManyToOne
	public Company getFromInternalcompany() {
		return fromInternalcompany;
	}

	public void setFromInternalcompany(Company fromInternalcompany) {
		this.fromInternalcompany = fromInternalcompany;
	}
	

}
